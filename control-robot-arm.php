#!/bin/env php
<?php

require_once('RobotArm.class.php');

function tokenize(string $command) {
    $tokens = preg_split('/\s+/', $command);

    if($tokens === FALSE) {
        throw new Exception("preg_split(): failed");
    }

    for($i = 0; $i < count($tokens); $i++) {
        $tokens[$i] = trim($tokens[$i]);
    }

    return $tokens;
}

function is_token_digit($token) {
    if(preg_match('/^[\d]+$/', $token)) {
        return true;
    } else {
        return false;
    }
}

function is_token_string($string, $token) {
    if(preg_match("/^$string$/i", $token)) {
        return true;
    } else {
        return false;
    }
}

function is_token_quit($token) { return is_token_string('quit', $token); }
function is_token_pile($token) { return is_token_string('pile', $token); }
function is_token_move($token) { return is_token_string('move', $token); }
function is_token_onto($token) { return is_token_string('onto', $token); }
function is_token_over($token) { return is_token_string('over', $token); }

/*
 * Define language and execute the appropriate logic for the command if valid.
 */
function parse_command($command, &$st) {
    $tokens = tokenize($command);
    $count = count($tokens);

    if($count == 1 &&
       is_token_digit($tokens[0])) {
        $st->init($tokens[0]);
    }

    else if($count == 1 &&
       is_token_quit($tokens[0])) {
        $st->quit();
    }

    else if($count == 4 &&
       is_token_move($tokens[0]) &&
       is_token_digit($tokens[1]) &&
       is_token_onto($tokens[2]) &&
       is_token_digit($tokens[3])) {
        $st->move_a_onto_b($tokens[1], $tokens[3]);
    }

    else if($count == 4 &&
       is_token_move($tokens[0]) &&
       is_token_digit($tokens[1]) &&
       is_token_over($tokens[2]) &&
       is_token_digit($tokens[3])) {
        $st->move_a_over_b($tokens[1], $tokens[3]);
    }

    else if($count == 4 &&
       is_token_pile($tokens[0]) &&
       is_token_digit($tokens[1]) &&
       is_token_onto($tokens[2]) &&
       is_token_digit($tokens[3])) {
        $st->pile_a_onto_b($tokens[1], $tokens[3]);
    }

    else if($count == 4 &&
       is_token_pile($tokens[0]) &&
       is_token_digit($tokens[1]) &&
       is_token_over($tokens[2]) &&
       is_token_digit($tokens[3])) {
        $st->pile_a_over_b($tokens[1], $tokens[3]);
    }

    else {
        return;
    }
}

function parse_one_set_of_commands($fp, &$st) {
    if($fp) {
        while(($line = fgets($fp, 512)) !== FALSE) {
            $line = trim($line);
            parse_command($line, $st);
        }

        if(!feof($fp)) {
            throw new Exception("fgets(): failed");
        }

        fclose($fp);
    }
}

if(PHP_SAPI == "cli") {
    if($argc == 1) {
        $fp = @fopen("php://stdin", "r");
        $st = new robot_arm();
        parse_one_set_of_commands($fp, $st);
    }
}

?>
