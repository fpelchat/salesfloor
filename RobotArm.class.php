<?php

class robot_arm {
    private $iteration = 0;
    private $quit = false;
    private $blockworld;

    function __construct() {}

    function init($n) {
        if($this->iteration != 0) {
            throw new Exception("First command must be the number of blocks.");
        }

        for($i = 0; $i < $n; $i++) {
            $this->blockworld[$i] = [$i];
        }

        ++$this->iteration;
    }

    function getBlockworld() {
        return $this->blockworld;
    }

    function quit() {
        if($this->iteration < 1) {
            throw new Exception("Initialize blockworld before quitting.");
        }

        $this->dump();
        $this->quit = true;

        ++$this->iteration;
    }

    function dump() {
        if(is_array($this->blockworld) && !empty($this->blockworld)) {
            for($i = 0; $i < count($this->blockworld); $i++) {
                printf("%d:%s\n", $i, (empty($this->blockworld)? '': (' ' . implode(' ', $this->blockworld[$i]))));
            }
        }
    }

    function debug() {
        for($i = 0; $i < count($this->blockworld); $i++) {
            $found = $this->find_block_position($i);
            printf("found block %d at i=%d, j=%d\n", $i, $found[0], $found[1]);
        }
    }

    function find_block_position($block) {
        for($i = 0; $i < count($this->blockworld); $i++) {
            for($j = 0; $j < count($this->blockworld[$i]); $j++) {
                if($block == $this->blockworld[$i][$j]) {
                    return [$i, $j];
                }
            }
        }

        throw new Exception("Can't find block=$block anywhere? (This should never happens unless there is a bug)");
    }

    function move_a_onto_b($a, $b) {
        if(empty($this->blockworld)) {
            throw new Exception("Must initialize blockworld before operating robot arm.");
        }

        $apos = $this->find_block_position($a);
        $bpos = $this->find_block_position($b);

        if($apos[0] != $bpos[0]) {
            $this->blockworld[$bpos[0]] = array_merge(
                array_slice($this->blockworld[$bpos[0]], 0, $bpos[1] + 1),
                [$a],
                array_slice($this->blockworld[$bpos[0]], $bpos[1] + 1, count($this->blockworld[$bpos[0]]) - $bpos[1] - 1)
            );

            $this->blockworld[$apos[0]] = array_merge(
                array_slice($this->blockworld[$apos[0]], 0, $apos[1]),
                array_slice($this->blockworld[$apos[0]], $apos[1] + 1, count($this->blockworld[$apos[0]]) - $apos[1] - 1)
            );
        } else {
            /*
             * Ignore command where a == b or where a is in the same stack as b.
             * In fact if a == b its gotta be in the same stack so ignoring
             * command a_X_b is sufficient to cover both cases.
             */
        }
    }

    function move_a_over_b($a, $b) {
        if(empty($this->blockworld)) {
            throw new Exception("Must initialize blockworld before operating robot arm.");
        }

        $apos = $this->find_block_position($a);
        $bpos = $this->find_block_position($b);

        if($apos[0] != $bpos[0]) {
            $this->blockworld[$bpos[0]] = array_merge(
                array_slice($this->blockworld[$bpos[0]], 0, count($this->blockworld[$bpos[0]])),
                [$a]
            );

            $this->blockworld[$apos[0]] = array_merge(
                array_slice($this->blockworld[$apos[0]], 0, $apos[1]),
                array_slice($this->blockworld[$apos[0]], $apos[1] + 1, count($this->blockworld[$apos[0]]) - $apos[1] - 1)
            );
        } else {
            /*
             * Ignore command where a == b or where a is in the same stack as b.
             * In fact if a == b its gotta be in the same stack so ignoring
             * command a_X_b is sufficient to cover both cases.
             */
        }
    }

    function pile_a_onto_b($a, $b) {
        if(empty($this->blockworld)) {
            throw new Exception("Must initialize blockworld before operating robot arm.");
        }

        $apos = $this->find_block_position($a);
        $bpos = $this->find_block_position($b);

        if($apos[0] != $bpos[0]) {
            $this->blockworld[$bpos[0]] = array_merge(
                array_slice($this->blockworld[$bpos[0]], 0, $bpos[1] + 1),
                array_slice($this->blockworld[$apos[0]], $apos[1], count($this->blockworld[$apos[0]]) - $apos[1]),
                array_slice($this->blockworld[$bpos[0]], $bpos[1] + 1, count($this->blockworld[$bpos[0]]) - $bpos[1] - 1)
            );

            $this->blockworld[$apos[0]] = array_merge(
                array_slice($this->blockworld[$apos[0]], 0, $apos[1])
            );
        } else {
            /*
             * Ignore command where a == b or where a is in the same stack as b.
             * In fact if a == b its gotta be in the same stack so ignoring
             * command a_X_b is sufficient to cover both cases.
             */
        }
    }

    function pile_a_over_b($a, $b) {
        if(empty($this->blockworld)) {
            throw new Exception("Must initialize blockworld before operating robot arm.");
        }

        $apos = $this->find_block_position($a);
        $bpos = $this->find_block_position($b);

        if($apos[0] != $bpos[0]) {
            $this->blockworld[$bpos[0]] = array_merge(
                array_slice($this->blockworld[$bpos[0]], 0, count($this->blockworld[$bpos[0]])),
                array_slice($this->blockworld[$apos[0]], $apos[1], count($this->blockworld[$apos[0]]) - $apos[1])
            );

            $this->blockworld[$apos[0]] = array_merge(
                array_slice($this->blockworld[$apos[0]], 0, $apos[1])
            );
        } else {
            /*
             * Ignore command where a == b or where a is in the same stack as b.
             * In fact if a == b its gotta be in the same stack so ignoring
             * command a_X_b is sufficient to cover both cases.
             */
        }
    }
}
