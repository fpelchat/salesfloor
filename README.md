I have a local install of phpunit 8.5.x.

For more information on how to install phpunit on your system you can go to https://phpunit.readthedocs.io/en/8.5/installation.html

To run the unit tests and ensure the robot arm operations are working as expected:

```
theghost@ip-171-34-22-66:~/salesfloor# phpunit RobotArmOpsTest.php
PHPUnit 8.5.2 by Sebastian Bergmann and contributors.

.....                                                               5 / 5 (100%)

Time: 34 ms, Memory: 4.00 MB

OK (5 tests, 66 assertions)
theghost@ip-171-34-22-66:~/salesfloor#
```

Ensure php is in your ${PATH}.

`chmod u+x control-robot-arm.php`

CLI example:

```
theghost@ip-171-34-22-66:~/salesfloor# ./control-robot-arm.php
10
move 9 onto 1
move 8 over 1
move 7 over 1
move 6 over 1
pile 8 over 6
pile 8 over 5
move 2 over 1
move 4 over 9
quit
0: 0
1: 1 9 2 4
2:
3: 3
4:
5: 5 8 7 6
6:
7:
8:
9:
theghost@ip-171-34-22-66:~/salesfloor#
```

Submitting a file:

```
theghost@ip-171-34-22-66:~/salesfloor# cat commands.txt | ./control-robot-arm.php
0: 0
1: 1 9 2 4
2:
3: 3
4:
5: 5 8 7 6
6:
7:
8:
9:
theghost@ip-171-34-22-66:~/salesfloor#
```
