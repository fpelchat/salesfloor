<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once('RobotArm.class.php');

final class RobotArmOpsTest extends TestCase {
    public function testClass(): void {
        $o = new robot_arm;
        $this->assertInstanceOf('robot_arm', $o);
        $this->assertIsCallable([$o, 'init']);
        $o->init(3);
        $this->assertIsCallable([$o, 'getBlockworld']);
        $this->assertIsArray($o->getBlockworld());
        $this->assertEquals(3, count($o->getBlockworld()));
    }

    public function testMoveOnto(): void {
        $o = new robot_arm;
        $o->init(3);
        $this->assertEquals($o->getBlockworld()[0], [0]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], [2]);
        $o->move_a_onto_b(2, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 2]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->move_a_onto_b(1, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 1, 2]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->move_a_onto_b(2, 0); //should be ignored a & b are in the same stack
        $this->assertEquals($o->getBlockworld()[0], [0, 1, 2]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
    }

    public function testMoveOver(): void {
        $o = new robot_arm;
        $o->init(3);
        $this->assertEquals($o->getBlockworld()[0], [0]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], [2]);
        $o->move_a_over_b(2, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 2]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->move_a_over_b(1, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 2, 1]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->move_a_over_b(2, 0); //should be ignored a & b are in the same stack
        $this->assertEquals($o->getBlockworld()[0], [0, 2, 1]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
    }

    public function testPileOnto(): void {
        $o = new robot_arm;
        $o->init(3);
        $this->assertEquals($o->getBlockworld()[0], [0]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], [2]);
        $o->pile_a_onto_b(2, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 2]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->pile_a_onto_b(0, 1);
        $this->assertEquals($o->getBlockworld()[0], []);
        $this->assertEquals($o->getBlockworld()[1], [1, 0, 2]);
        $this->assertEquals($o->getBlockworld()[2], []);
        $o->pile_a_onto_b(2, 0); //should be ignored a & b are in the same stack
        $this->assertEquals($o->getBlockworld()[0], []);
        $this->assertEquals($o->getBlockworld()[1], [1, 0, 2]);
        $this->assertEquals($o->getBlockworld()[2], []);
    }

    public function testPileOver(): void {
        $o = new robot_arm;
        $o->init(5);
        $this->assertEquals($o->getBlockworld()[0], [0]);
        $this->assertEquals($o->getBlockworld()[1], [1]);
        $this->assertEquals($o->getBlockworld()[2], [2]);
        $this->assertEquals($o->getBlockworld()[3], [3]);
        $this->assertEquals($o->getBlockworld()[4], [4]);
        $o->pile_a_over_b(1, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 1]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], [2]);
        $this->assertEquals($o->getBlockworld()[3], [3]);
        $this->assertEquals($o->getBlockworld()[4], [4]);
        $o->pile_a_over_b(2, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 1, 2]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
        $this->assertEquals($o->getBlockworld()[3], [3]);
        $this->assertEquals($o->getBlockworld()[4], [4]);
        $o->pile_a_over_b(3, 0);
        $this->assertEquals($o->getBlockworld()[0], [0, 1, 2, 3]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
        $this->assertEquals($o->getBlockworld()[3], []);
        $this->assertEquals($o->getBlockworld()[4], [4]);
        $o->pile_a_over_b(1, 4);
        $this->assertEquals($o->getBlockworld()[0], [0]);
        $this->assertEquals($o->getBlockworld()[1], []);
        $this->assertEquals($o->getBlockworld()[2], []);
        $this->assertEquals($o->getBlockworld()[3], []);
        $this->assertEquals($o->getBlockworld()[4], [4, 1, 2, 3]);
    }
}
